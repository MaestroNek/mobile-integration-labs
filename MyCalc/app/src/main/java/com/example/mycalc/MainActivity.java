package com.example.mycalc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    TextView display;
    Button[] buttons = new Button[16];
    Calculator calculator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        display = findViewById(R.id.numberInput);
        buttons[0] = findViewById(R.id.button_0);
        buttons[1] = findViewById(R.id.button_1);
        buttons[2] = findViewById(R.id.button_2);
        buttons[3] = findViewById(R.id.button_3);
        buttons[4] = findViewById(R.id.button_4);
        buttons[5] = findViewById(R.id.button_5);
        buttons[6] = findViewById(R.id.button_6);
        buttons[7] = findViewById(R.id.button_7);
        buttons[8] = findViewById(R.id.button_8);
        buttons[9] = findViewById(R.id.button_9);
        buttons[10] = findViewById(R.id.button_C);
        buttons[11] = findViewById(R.id.button_plus);
        buttons[12] = findViewById(R.id.button_minus);
        buttons[13] = findViewById(R.id.button_multy);
        buttons[14] = findViewById(R.id.button_divide);
        buttons[15] = findViewById(R.id.button_Enter);

        for(Button button: buttons){
            button.setOnClickListener(clickListener);
        }

        calculator = new Calculator();
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Button button = (Button) view;
            try {
                pushButton(button.getText().toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    private void pushButton(String sign) throws IOException {
        if (sign.equals("C")){
            display.setText("0");
            calculator.first = 0;
            calculator.second = 0;
            calculator.operation = "";
        } else if ("0123456789".contains(sign)) {
            if (calculator.second == Integer.parseInt(display.getText().toString())) {
                calculator.second = Integer.parseInt(display.getText().toString() + sign);
            } else {
                calculator.second = Integer.parseInt(sign);
            }
            display.setText(Integer.toString(calculator.second));
        } else {
            calculator.first = calculator.calculate();
            display.setText(Integer.toString(calculator.first));
            calculator.operation = sign;
            calculator.second = 0;
        }
    }
}
