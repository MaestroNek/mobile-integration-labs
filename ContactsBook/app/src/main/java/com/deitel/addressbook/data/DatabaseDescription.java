package com.deitel.addressbook.data;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class DatabaseDescription {


    public static final String AUTHORITY = "com.deitel.addressbook.data";

    private static final Uri BASE_CONTENT_URI =Uri.parse("content://" + AUTHORITY);

    public static final class Contact implements BaseColumns{

        public static final String TABLE_NAME = "contacts";

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_SURNAME = "surname";
        public static final String COLUMN_LASTNAME = "lastname";
        public static final String COLUMN_PHONE = "phone";
        public static final String COLUMN_EMAIL = "email";

        public static Uri buildContactUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

}
